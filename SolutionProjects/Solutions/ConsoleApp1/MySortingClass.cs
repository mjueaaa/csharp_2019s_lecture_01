﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1 {    

    public class MySortingClass {
        List<double> list = new List<double>();

        public MySortingClass(int size) {
            Random rnd = new Random();
            for (int i = 0; i < size; ++i)
                list.Add(rnd.Next(0, 100));
        }

        public MySortingClass Sort() {
            // This is a slow bubble sort. What is wrong? 
            bool swap = true;
            while (swap) {
                swap = false;
                for (int j = 0; j < list.Count - 1; ++j) {
                    if (list[j] > list[j + 1]) {
                        double tmp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = tmp;
                        swap = true;
                    }
                }
            }
            return this;
        }

        public MySortingClass SortEx() {
            list.Sort();
            return this;
        }

        public override string ToString() {
            string res = "";
            for (int i = 0; i < list.Count; ++i) {
                if (i > 0 && i % 10 == 0)
                    res += "\n";
                res += $"{list[i]}, ";
            }
            return res;
        }
    }
}
