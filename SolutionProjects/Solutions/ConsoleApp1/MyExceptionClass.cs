﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1 {

    class MyExceptionClass {

        private void MyMethodWithError(int num = 0) {
            throw new Exception("Something horrible happend! " + num);
        }

        public void MyNormalMethod(int num = 0) {
            try {
                MyMethodWithError(num);
            } catch (Exception ex) {
                Console.WriteLine("EXCEPTION: " + ex.Message);
            } finally {
                Console.WriteLine("Done calling MyMethodWithError");
            }
        }

        public void StrangeMethod(int arg = 0) {
            int res = 1;
            res = res / arg;
        }

    }

}
