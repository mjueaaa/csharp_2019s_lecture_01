﻿using ClassLibrary1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1 {

    public class MyClass {
        private int MyArg;

        public MyClass(int arg) {
            MyArg = arg;
        }

        public override string ToString() {
            return $"MyArg set to {MyArg}";
        }

        public int Arg {
            get {
                return MyArg;
            }
            set {
                MyArg = value;
            }
        }
    }

    public class MyExternalClassEx : MyExternalClass {

        public MyExternalClassEx() : base("MyExternalClassEx") {
        }

        public override string ToString() {
            return base.ToString() + " (ToString Called from MyExternalClassEx)";
        }
    }

    class Program {
        static void Exercise3(string[] args) {
            MyClass m1 = new MyClass(23);
            Console.WriteLine(m1);

            Console.ReadKey();
        }

        static void Exercise4(string[] args) {            
            var m = new MyClass(56);
            m.Arg = 65;
            Console.WriteLine(m);

            Console.ReadKey();
        }

        static void Exercise5(string[] args) {
            IMyExternalClass ex1 = new MyExternalClass();
            Console.WriteLine(ex1.GetMeAsText());
            Console.WriteLine(ex1);


            MyExternalClassEx ex2 = new MyExternalClassEx();
            Console.WriteLine(ex2);
            Console.ReadKey();
        }

        static void Exercise6(string[] args) {
            MySortingClass c = new MySortingClass(15);
            Console.WriteLine(c);

            Console.WriteLine(c.Sort());

            Console.WriteLine((new MySortingClass(20)).SortEx());
            Console.ReadKey();
        }

        static void Exercise7(string[] args) {
            List<double> list = new List<double>();
            Random rnd = new Random();

            int size_of_list = 20;
            for (int i = 0; i < size_of_list; ++i)
                list.Add(rnd.Next(0, 100));

            foreach (var d in list)
                Console.WriteLine(d);
            Console.ReadKey();
        }

        static void Exercise8(string[] args) {
            MyExceptionClass ex = new MyExceptionClass();

            ex.MyNormalMethod();
            //ex.StrangeMethod();

            Console.ReadKey();
        }

        static void Main(string[] args) {
            Exercise3(args);
            Exercise4(args);
            Exercise5(args);
            Exercise6(args);
            Exercise7(args);
            Exercise8(args);
        }
    }
}
