﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public interface IMyExternalClass {
        string GetMeAsText();
    }

    public class MyExternalClass : IMyExternalClass {
        private string Arg = "initial value";

        public MyExternalClass(string arg = "default argument") {
            Arg = arg;
        }

        public override string ToString() {
            return $"MyExternalClass : Arg set to {Arg}";
        }

        string IMyExternalClass.GetMeAsText() {
            return "" + this;
        }
    }
}
