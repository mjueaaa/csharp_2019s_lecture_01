﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApp0 {
    
    public class MyClass {

        

        public void StrangeMethod(int arg=0) {
            int res = 1;
            res = res / arg;
        }

    }

    

    class Program {        

        public void DefineVariables() {

            int i;
            int j = 0;

            double[] d1 = null;
            double[] d2 = new double[10];
            double[] d3 = new double[] { 2, 3, 4 };
            double[] d4 = { 3, 6, 9 };
            double[] d5 = new[] { 55.0, 77.0, 11.4 };

            string s1 = "Hello world";
            string s2 = @"c:\user\pictures";
            string s3 = $"Der er {3} spor efter kattepoter i min lagkage";

            MyClass q1 = new MyClass();
            var q2 = new MyClass();     // implicit type

        }

        public void Controltructures() {
            for (int i = 0; i < 10; ++i)
                Console.WriteLine($"i = {i}");

            int[] numbers = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int j = 0;
            while (true) {
                Console.WriteLine($"j = {numbers[j]}");
                if (j++ >= 9)
                    break;
            }
            
            foreach(var n in numbers)
                Console.WriteLine($"n = {n}");
        }

        // Classes, Inheritance and interfaces
        public class CoolBanana {
            private int value;
            public CoolBanana(int i) {
                Console.WriteLine("Created CoolBanana with value " + i);
                this.value = i; // or just : value = i;
            }
        }
        public interface IStrangeMethod {
            string StrangeMethod(int arg);
        }
        public class LessCoolBanana : CoolBanana, IStrangeMethod {
            public LessCoolBanana() : base(4) {
                Console.WriteLine("Created LessCoolBanana");
            }
            public string StrangeMethod(int arg) {
                Console.WriteLine("Why would you ever call a strange method??");
                return "FunnyBones";
            }
        }

        static void Main(string[] args) {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.InvariantCulture;

            // Variables and control structures
            (new Program()).DefineVariables();
            (new Program()).Controltructures();

            // Classes, Inheritance and interfaces
            var c1 = new CoolBanana(2);
            var c2 = new LessCoolBanana();
            Console.WriteLine(c2.StrangeMethod(33));

            var m = new MyClass();            
            //m.StrangeMethod();

            Console.ReadKey();
        }
    }
}
